import WebSocket from 'ws';

const PORT_WS = 8001;
const wss = new WebSocket.Server({port: PORT_WS});


console.log(`WebSocket server listening on port :${PORT_WS}`);

let streams = [];

let coords = {
    x: 0,
    y: 0
};


wss.on('connection', (ws) => {

    console.log('Client connected');
    ws.isAlive = true;

    ws.on('message', (JSONmessage) => {

        let message;

        try {
            message = JSON.parse(JSONmessage);
        }
        catch (error) {
            console.error('JSON parse failed. [message: ', message, "]");
            return;
        }

        if (message.method === "setCoords") {
            coords = message.data;
            return;
        }

        if (message.method === "subscribe") {

            let id = message.resource.slice(-2);
            startStream(ws, id)
        }
    });

    ws.on('close', () => {
        console.log('Client disconected');
        ws.isAlive = false;
    });
});


const startStream = (ws, id) => {

    // streams.push(id, )

    let loop = setInterval(() => {

        if (ws.isAlive === false) {
            clearInterval(loop);
            return;
        }

        ws.send(JSON.stringify({
            body: {
                datastreams: [
                    {
                        id: "posX",
                        current_value: coords.x.toString()
                    },
                    {
                        id: "posY",
                        current_value: coords.y.toString()
                    },
                    {
                        id: "posZ",
                        current_value: "0"
                    },
                    {
                        id: "batLevel",
                        current_value: getRndInteger(1, 100).toString()
                    },
                    {
                        id: "temperature",
                        current_value: getRndInteger(-10, 40).toString()
                    },
                    {
                        id: "pressure",
                        current_value: getRndInteger(950000, 105000).toString()
                    },
                    {
                        id: 'acc',
                        current_value: '524;-108;-16980'
                    },
                ],
                id
            },
            resource: "/feeds/" + id
        }));
    }, 1000);
};

const getRndInteger = (min, max) => {
    return Math.floor(Math.random() * (max - min + 1) ) + min;
};
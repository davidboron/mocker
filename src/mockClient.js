const SERVER_IP = 'localhost';
const PORT = 8001;
let ws = new WebSocket("ws://" + SERVER_IP + ":" + PORT);

const SCALE = "50.35747216309493";


ws.onopen = function () {
    console.log('Connected to WebSocket')
    // ws.send(JSON.stringify(message));
};

ws.onerror = function (e) {
    console.error("Error occured: ", e);
};

ws.onclose = function (e) {
    console.error("Closing websocket: ", e);
};


let markers = [];

let body = document.getElementsByTagName("body")[0];
let svg = document.getElementsByTagName("svg")[0];


let button = document.createElement("button");
button.innerHTML = "Add marker";
body.appendChild(button);

button.addEventListener("click", function () {
    createMarker();
});


const createMarker = () => {
    let svgImg = document.createElementNS('http://www.w3.org/2000/svg', 'image');
    svgImg.setAttributeNS(null, 'x', '100');
    svgImg.setAttributeNS(null, 'y', '100');
    svgImg.setAttributeNS(null, 'width', '50');
    svgImg.setAttributeNS('http://www.w3.org/1999/xlink', 'href', './img/placeholder.svg');
    svgImg.setAttributeNS(null, "draggable", true);
    svgImg.addEventListener("mousedown", drag);
    svgImg.addEventListener("mouseup", deselectElement);
    svgImg.addEventListener("mouseout", deselectElement);
    svgImg.classList.add("marker");
    markers.push(svgImg);
    svg.appendChild(svgImg);
};

createMarker();


function drag(evt){
    let selectedElement = evt.target;
    selectedElement.addEventListener("mousemove", moveElement);
}

function moveElement(evt){
    let selectedElement = evt.target;
    let loc = cursorPoint(evt);
    let coords = {
        x: loc.x-20,
        y: loc.y-20,
    };

    selectedElement.setAttributeNS(null, "x", coords.x);
    selectedElement.setAttributeNS(null, "y", coords.y);

    let message = {
        "method": "setCoords",
        "data": {
            "x": coords.x / SCALE,
            "y": coords.y / SCALE
        }
    };
    ws.send(JSON.stringify(message));
}

function deselectElement(evt){
    evt.target.removeEventListener("mousemove", moveElement);
}


// Create an SVGPoint for future math
var pt = svg.createSVGPoint();

// Get point in global SVG space
function cursorPoint(evt){
    pt.x = evt.clientX; pt.y = evt.clientY;
    return pt.matrixTransform(svg.getScreenCTM().inverse());
}
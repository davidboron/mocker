var path = require('path');

module.exports = {
    mode: "development",
    entry: './src/mockClient.js',
    output: {
        path: path.resolve(__dirname, 'public/dist'),
        filename: "bundle.js",
        publicPath: "/dist/"
    },
    devtool: 'inline-source-map',
    module: {
        rules: [
            {
                use: 'babel-loader',
                test: /\.js$/,
                exclude: /node_modules/
            }
        ]
    },
    devServer: {
        inline: true,
        port: 8000
    }
};
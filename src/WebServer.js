import express from "express";
import cors from "cors";
import {allFeeds} from "../data/allFeeds";
import {zones} from "../data/zones";

const PORT_WEB = 80;
const web = express();

web.use(cors());
web.use(express.static('public'));

web.listen(PORT_WEB, () => console.log(`Web server listening on port :${PORT_WEB}`));

web.get('/sensmapserver/api/feeds', (req, res) => res.send(allFeeds));

web.get('/sensmapserver/api/buildings/:id/plans/:plan/zones', (req, res) => res.send(zones));
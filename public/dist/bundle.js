/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/dist/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/mockClient.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/mockClient.js":
/*!***************************!*\
  !*** ./src/mockClient.js ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var SERVER_IP = 'localhost';
var PORT = 8001;
var ws = new WebSocket("ws://" + SERVER_IP + ":" + PORT);

var SCALE = "50.35747216309493";

ws.onopen = function () {
    console.log('Connected to WebSocket');
    // ws.send(JSON.stringify(message));
};

ws.onerror = function (e) {
    console.error("Error occured: ", e);
};

ws.onclose = function (e) {
    console.error("Closing websocket: ", e);
};

var markers = [];

var body = document.getElementsByTagName("body")[0];
var svg = document.getElementsByTagName("svg")[0];

var button = document.createElement("button");
button.innerHTML = "Add marker";
body.appendChild(button);

button.addEventListener("click", function () {
    createMarker();
});

var createMarker = function createMarker() {
    var svgImg = document.createElementNS('http://www.w3.org/2000/svg', 'image');
    svgImg.setAttributeNS(null, 'x', '100');
    svgImg.setAttributeNS(null, 'y', '100');
    svgImg.setAttributeNS(null, 'width', '50');
    svgImg.setAttributeNS('http://www.w3.org/1999/xlink', 'href', './img/placeholder.svg');
    svgImg.setAttributeNS(null, "draggable", true);
    svgImg.addEventListener("mousedown", drag);
    svgImg.addEventListener("mouseup", deselectElement);
    svgImg.addEventListener("mouseout", deselectElement);
    svgImg.classList.add("marker");
    markers.push(svgImg);
    svg.appendChild(svgImg);
};

createMarker();

function drag(evt) {
    var selectedElement = evt.target;
    selectedElement.addEventListener("mousemove", moveElement);
}

function moveElement(evt) {
    var selectedElement = evt.target;
    var loc = cursorPoint(evt);
    var coords = {
        x: loc.x - 20,
        y: loc.y - 20
    };

    selectedElement.setAttributeNS(null, "x", coords.x);
    selectedElement.setAttributeNS(null, "y", coords.y);

    var message = {
        "method": "setCoords",
        "data": {
            "x": coords.x / SCALE,
            "y": coords.y / SCALE
        }
    };
    ws.send(JSON.stringify(message));
}

function deselectElement(evt) {
    evt.target.removeEventListener("mousemove", moveElement);
}

// Create an SVGPoint for future math
var pt = svg.createSVGPoint();

// Get point in global SVG space
function cursorPoint(evt) {
    pt.x = evt.clientX;pt.y = evt.clientY;
    return pt.matrixTransform(svg.getScreenCTM().inverse());
}

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL21vY2tDbGllbnQuanMiXSwibmFtZXMiOlsiU0VSVkVSX0lQIiwiUE9SVCIsIndzIiwiV2ViU29ja2V0IiwiU0NBTEUiLCJvbm9wZW4iLCJjb25zb2xlIiwibG9nIiwib25lcnJvciIsImUiLCJlcnJvciIsIm9uY2xvc2UiLCJtYXJrZXJzIiwiYm9keSIsImRvY3VtZW50IiwiZ2V0RWxlbWVudHNCeVRhZ05hbWUiLCJzdmciLCJidXR0b24iLCJjcmVhdGVFbGVtZW50IiwiaW5uZXJIVE1MIiwiYXBwZW5kQ2hpbGQiLCJhZGRFdmVudExpc3RlbmVyIiwiY3JlYXRlTWFya2VyIiwic3ZnSW1nIiwiY3JlYXRlRWxlbWVudE5TIiwic2V0QXR0cmlidXRlTlMiLCJkcmFnIiwiZGVzZWxlY3RFbGVtZW50IiwiY2xhc3NMaXN0IiwiYWRkIiwicHVzaCIsImV2dCIsInNlbGVjdGVkRWxlbWVudCIsInRhcmdldCIsIm1vdmVFbGVtZW50IiwibG9jIiwiY3Vyc29yUG9pbnQiLCJjb29yZHMiLCJ4IiwieSIsIm1lc3NhZ2UiLCJzZW5kIiwiSlNPTiIsInN0cmluZ2lmeSIsInJlbW92ZUV2ZW50TGlzdGVuZXIiLCJwdCIsImNyZWF0ZVNWR1BvaW50IiwiY2xpZW50WCIsImNsaWVudFkiLCJtYXRyaXhUcmFuc2Zvcm0iLCJnZXRTY3JlZW5DVE0iLCJpbnZlcnNlIl0sIm1hcHBpbmdzIjoiO0FBQUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7QUFDQSx5REFBaUQsY0FBYztBQUMvRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBMkIsMEJBQTBCLEVBQUU7QUFDdkQseUNBQWlDLGVBQWU7QUFDaEQ7QUFDQTtBQUNBOztBQUVBO0FBQ0EsOERBQXNELCtEQUErRDs7QUFFckg7QUFDQTs7O0FBR0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7O0FDbkVBLElBQU1BLFlBQVksV0FBbEI7QUFDQSxJQUFNQyxPQUFPLElBQWI7QUFDQSxJQUFJQyxLQUFLLElBQUlDLFNBQUosQ0FBYyxVQUFVSCxTQUFWLEdBQXNCLEdBQXRCLEdBQTRCQyxJQUExQyxDQUFUOztBQUVBLElBQU1HLFFBQVEsbUJBQWQ7O0FBR0FGLEdBQUdHLE1BQUgsR0FBWSxZQUFZO0FBQ3BCQyxZQUFRQyxHQUFSLENBQVksd0JBQVo7QUFDQTtBQUNILENBSEQ7O0FBS0FMLEdBQUdNLE9BQUgsR0FBYSxVQUFVQyxDQUFWLEVBQWE7QUFDdEJILFlBQVFJLEtBQVIsQ0FBYyxpQkFBZCxFQUFpQ0QsQ0FBakM7QUFDSCxDQUZEOztBQUlBUCxHQUFHUyxPQUFILEdBQWEsVUFBVUYsQ0FBVixFQUFhO0FBQ3RCSCxZQUFRSSxLQUFSLENBQWMscUJBQWQsRUFBcUNELENBQXJDO0FBQ0gsQ0FGRDs7QUFLQSxJQUFJRyxVQUFVLEVBQWQ7O0FBRUEsSUFBSUMsT0FBT0MsU0FBU0Msb0JBQVQsQ0FBOEIsTUFBOUIsRUFBc0MsQ0FBdEMsQ0FBWDtBQUNBLElBQUlDLE1BQU1GLFNBQVNDLG9CQUFULENBQThCLEtBQTlCLEVBQXFDLENBQXJDLENBQVY7O0FBR0EsSUFBSUUsU0FBU0gsU0FBU0ksYUFBVCxDQUF1QixRQUF2QixDQUFiO0FBQ0FELE9BQU9FLFNBQVAsR0FBbUIsWUFBbkI7QUFDQU4sS0FBS08sV0FBTCxDQUFpQkgsTUFBakI7O0FBRUFBLE9BQU9JLGdCQUFQLENBQXdCLE9BQXhCLEVBQWlDLFlBQVk7QUFDekNDO0FBQ0gsQ0FGRDs7QUFLQSxJQUFNQSxlQUFlLFNBQWZBLFlBQWUsR0FBTTtBQUN2QixRQUFJQyxTQUFTVCxTQUFTVSxlQUFULENBQXlCLDRCQUF6QixFQUF1RCxPQUF2RCxDQUFiO0FBQ0FELFdBQU9FLGNBQVAsQ0FBc0IsSUFBdEIsRUFBNEIsR0FBNUIsRUFBaUMsS0FBakM7QUFDQUYsV0FBT0UsY0FBUCxDQUFzQixJQUF0QixFQUE0QixHQUE1QixFQUFpQyxLQUFqQztBQUNBRixXQUFPRSxjQUFQLENBQXNCLElBQXRCLEVBQTRCLE9BQTVCLEVBQXFDLElBQXJDO0FBQ0FGLFdBQU9FLGNBQVAsQ0FBc0IsOEJBQXRCLEVBQXNELE1BQXRELEVBQThELHVCQUE5RDtBQUNBRixXQUFPRSxjQUFQLENBQXNCLElBQXRCLEVBQTRCLFdBQTVCLEVBQXlDLElBQXpDO0FBQ0FGLFdBQU9GLGdCQUFQLENBQXdCLFdBQXhCLEVBQXFDSyxJQUFyQztBQUNBSCxXQUFPRixnQkFBUCxDQUF3QixTQUF4QixFQUFtQ00sZUFBbkM7QUFDQUosV0FBT0YsZ0JBQVAsQ0FBd0IsVUFBeEIsRUFBb0NNLGVBQXBDO0FBQ0FKLFdBQU9LLFNBQVAsQ0FBaUJDLEdBQWpCLENBQXFCLFFBQXJCO0FBQ0FqQixZQUFRa0IsSUFBUixDQUFhUCxNQUFiO0FBQ0FQLFFBQUlJLFdBQUosQ0FBZ0JHLE1BQWhCO0FBQ0gsQ0FiRDs7QUFlQUQ7O0FBR0EsU0FBU0ksSUFBVCxDQUFjSyxHQUFkLEVBQWtCO0FBQ2QsUUFBSUMsa0JBQWtCRCxJQUFJRSxNQUExQjtBQUNBRCxvQkFBZ0JYLGdCQUFoQixDQUFpQyxXQUFqQyxFQUE4Q2EsV0FBOUM7QUFDSDs7QUFFRCxTQUFTQSxXQUFULENBQXFCSCxHQUFyQixFQUF5QjtBQUNyQixRQUFJQyxrQkFBa0JELElBQUlFLE1BQTFCO0FBQ0EsUUFBSUUsTUFBTUMsWUFBWUwsR0FBWixDQUFWO0FBQ0EsUUFBSU0sU0FBUztBQUNUQyxXQUFHSCxJQUFJRyxDQUFKLEdBQU0sRUFEQTtBQUVUQyxXQUFHSixJQUFJSSxDQUFKLEdBQU07QUFGQSxLQUFiOztBQUtBUCxvQkFBZ0JQLGNBQWhCLENBQStCLElBQS9CLEVBQXFDLEdBQXJDLEVBQTBDWSxPQUFPQyxDQUFqRDtBQUNBTixvQkFBZ0JQLGNBQWhCLENBQStCLElBQS9CLEVBQXFDLEdBQXJDLEVBQTBDWSxPQUFPRSxDQUFqRDs7QUFFQSxRQUFJQyxVQUFVO0FBQ1Ysa0JBQVUsV0FEQTtBQUVWLGdCQUFRO0FBQ0osaUJBQUtILE9BQU9DLENBQVAsR0FBV2xDLEtBRFo7QUFFSixpQkFBS2lDLE9BQU9FLENBQVAsR0FBV25DO0FBRlo7QUFGRSxLQUFkO0FBT0FGLE9BQUd1QyxJQUFILENBQVFDLEtBQUtDLFNBQUwsQ0FBZUgsT0FBZixDQUFSO0FBQ0g7O0FBRUQsU0FBU2IsZUFBVCxDQUF5QkksR0FBekIsRUFBNkI7QUFDekJBLFFBQUlFLE1BQUosQ0FBV1csbUJBQVgsQ0FBK0IsV0FBL0IsRUFBNENWLFdBQTVDO0FBQ0g7O0FBR0Q7QUFDQSxJQUFJVyxLQUFLN0IsSUFBSThCLGNBQUosRUFBVDs7QUFFQTtBQUNBLFNBQVNWLFdBQVQsQ0FBcUJMLEdBQXJCLEVBQXlCO0FBQ3JCYyxPQUFHUCxDQUFILEdBQU9QLElBQUlnQixPQUFYLENBQW9CRixHQUFHTixDQUFILEdBQU9SLElBQUlpQixPQUFYO0FBQ3BCLFdBQU9ILEdBQUdJLGVBQUgsQ0FBbUJqQyxJQUFJa0MsWUFBSixHQUFtQkMsT0FBbkIsRUFBbkIsQ0FBUDtBQUNILEMiLCJmaWxlIjoiYnVuZGxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7XG4gXHRcdFx0XHRjb25maWd1cmFibGU6IGZhbHNlLFxuIFx0XHRcdFx0ZW51bWVyYWJsZTogdHJ1ZSxcbiBcdFx0XHRcdGdldDogZ2V0dGVyXG4gXHRcdFx0fSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiL2Rpc3QvXCI7XG5cblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSBcIi4vc3JjL21vY2tDbGllbnQuanNcIik7XG4iLCJjb25zdCBTRVJWRVJfSVAgPSAnbG9jYWxob3N0JztcclxuY29uc3QgUE9SVCA9IDgwMDE7XHJcbmxldCB3cyA9IG5ldyBXZWJTb2NrZXQoXCJ3czovL1wiICsgU0VSVkVSX0lQICsgXCI6XCIgKyBQT1JUKTtcclxuXHJcbmNvbnN0IFNDQUxFID0gXCI1MC4zNTc0NzIxNjMwOTQ5M1wiO1xyXG5cclxuXHJcbndzLm9ub3BlbiA9IGZ1bmN0aW9uICgpIHtcclxuICAgIGNvbnNvbGUubG9nKCdDb25uZWN0ZWQgdG8gV2ViU29ja2V0JylcclxuICAgIC8vIHdzLnNlbmQoSlNPTi5zdHJpbmdpZnkobWVzc2FnZSkpO1xyXG59O1xyXG5cclxud3Mub25lcnJvciA9IGZ1bmN0aW9uIChlKSB7XHJcbiAgICBjb25zb2xlLmVycm9yKFwiRXJyb3Igb2NjdXJlZDogXCIsIGUpO1xyXG59O1xyXG5cclxud3Mub25jbG9zZSA9IGZ1bmN0aW9uIChlKSB7XHJcbiAgICBjb25zb2xlLmVycm9yKFwiQ2xvc2luZyB3ZWJzb2NrZXQ6IFwiLCBlKTtcclxufTtcclxuXHJcblxyXG5sZXQgbWFya2VycyA9IFtdO1xyXG5cclxubGV0IGJvZHkgPSBkb2N1bWVudC5nZXRFbGVtZW50c0J5VGFnTmFtZShcImJvZHlcIilbMF07XHJcbmxldCBzdmcgPSBkb2N1bWVudC5nZXRFbGVtZW50c0J5VGFnTmFtZShcInN2Z1wiKVswXTtcclxuXHJcblxyXG5sZXQgYnV0dG9uID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImJ1dHRvblwiKTtcclxuYnV0dG9uLmlubmVySFRNTCA9IFwiQWRkIG1hcmtlclwiO1xyXG5ib2R5LmFwcGVuZENoaWxkKGJ1dHRvbik7XHJcblxyXG5idXR0b24uYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsIGZ1bmN0aW9uICgpIHtcclxuICAgIGNyZWF0ZU1hcmtlcigpO1xyXG59KTtcclxuXHJcblxyXG5jb25zdCBjcmVhdGVNYXJrZXIgPSAoKSA9PiB7XHJcbiAgICBsZXQgc3ZnSW1nID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudE5TKCdodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZycsICdpbWFnZScpO1xyXG4gICAgc3ZnSW1nLnNldEF0dHJpYnV0ZU5TKG51bGwsICd4JywgJzEwMCcpO1xyXG4gICAgc3ZnSW1nLnNldEF0dHJpYnV0ZU5TKG51bGwsICd5JywgJzEwMCcpO1xyXG4gICAgc3ZnSW1nLnNldEF0dHJpYnV0ZU5TKG51bGwsICd3aWR0aCcsICc1MCcpO1xyXG4gICAgc3ZnSW1nLnNldEF0dHJpYnV0ZU5TKCdodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rJywgJ2hyZWYnLCAnLi9pbWcvcGxhY2Vob2xkZXIuc3ZnJyk7XHJcbiAgICBzdmdJbWcuc2V0QXR0cmlidXRlTlMobnVsbCwgXCJkcmFnZ2FibGVcIiwgdHJ1ZSk7XHJcbiAgICBzdmdJbWcuYWRkRXZlbnRMaXN0ZW5lcihcIm1vdXNlZG93blwiLCBkcmFnKTtcclxuICAgIHN2Z0ltZy5hZGRFdmVudExpc3RlbmVyKFwibW91c2V1cFwiLCBkZXNlbGVjdEVsZW1lbnQpO1xyXG4gICAgc3ZnSW1nLmFkZEV2ZW50TGlzdGVuZXIoXCJtb3VzZW91dFwiLCBkZXNlbGVjdEVsZW1lbnQpO1xyXG4gICAgc3ZnSW1nLmNsYXNzTGlzdC5hZGQoXCJtYXJrZXJcIik7XHJcbiAgICBtYXJrZXJzLnB1c2goc3ZnSW1nKTtcclxuICAgIHN2Zy5hcHBlbmRDaGlsZChzdmdJbWcpO1xyXG59O1xyXG5cclxuY3JlYXRlTWFya2VyKCk7XHJcblxyXG5cclxuZnVuY3Rpb24gZHJhZyhldnQpe1xyXG4gICAgbGV0IHNlbGVjdGVkRWxlbWVudCA9IGV2dC50YXJnZXQ7XHJcbiAgICBzZWxlY3RlZEVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lcihcIm1vdXNlbW92ZVwiLCBtb3ZlRWxlbWVudCk7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIG1vdmVFbGVtZW50KGV2dCl7XHJcbiAgICBsZXQgc2VsZWN0ZWRFbGVtZW50ID0gZXZ0LnRhcmdldDtcclxuICAgIGxldCBsb2MgPSBjdXJzb3JQb2ludChldnQpO1xyXG4gICAgbGV0IGNvb3JkcyA9IHtcclxuICAgICAgICB4OiBsb2MueC0yMCxcclxuICAgICAgICB5OiBsb2MueS0yMCxcclxuICAgIH07XHJcblxyXG4gICAgc2VsZWN0ZWRFbGVtZW50LnNldEF0dHJpYnV0ZU5TKG51bGwsIFwieFwiLCBjb29yZHMueCk7XHJcbiAgICBzZWxlY3RlZEVsZW1lbnQuc2V0QXR0cmlidXRlTlMobnVsbCwgXCJ5XCIsIGNvb3Jkcy55KTtcclxuXHJcbiAgICBsZXQgbWVzc2FnZSA9IHtcclxuICAgICAgICBcIm1ldGhvZFwiOiBcInNldENvb3Jkc1wiLFxyXG4gICAgICAgIFwiZGF0YVwiOiB7XHJcbiAgICAgICAgICAgIFwieFwiOiBjb29yZHMueCAvIFNDQUxFLFxyXG4gICAgICAgICAgICBcInlcIjogY29vcmRzLnkgLyBTQ0FMRVxyXG4gICAgICAgIH1cclxuICAgIH07XHJcbiAgICB3cy5zZW5kKEpTT04uc3RyaW5naWZ5KG1lc3NhZ2UpKTtcclxufVxyXG5cclxuZnVuY3Rpb24gZGVzZWxlY3RFbGVtZW50KGV2dCl7XHJcbiAgICBldnQudGFyZ2V0LnJlbW92ZUV2ZW50TGlzdGVuZXIoXCJtb3VzZW1vdmVcIiwgbW92ZUVsZW1lbnQpO1xyXG59XHJcblxyXG5cclxuLy8gQ3JlYXRlIGFuIFNWR1BvaW50IGZvciBmdXR1cmUgbWF0aFxyXG52YXIgcHQgPSBzdmcuY3JlYXRlU1ZHUG9pbnQoKTtcclxuXHJcbi8vIEdldCBwb2ludCBpbiBnbG9iYWwgU1ZHIHNwYWNlXHJcbmZ1bmN0aW9uIGN1cnNvclBvaW50KGV2dCl7XHJcbiAgICBwdC54ID0gZXZ0LmNsaWVudFg7IHB0LnkgPSBldnQuY2xpZW50WTtcclxuICAgIHJldHVybiBwdC5tYXRyaXhUcmFuc2Zvcm0oc3ZnLmdldFNjcmVlbkNUTSgpLmludmVyc2UoKSk7XHJcbn0iXSwic291cmNlUm9vdCI6IiJ9
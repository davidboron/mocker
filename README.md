#Mocker
Node.js application serves as WebSocket mock server. It sends position data based on user interaction by drag'n'drop. 
  
 
[Bitbucket repository](https://bitbucket.org/davidboron/mocker/)
  
###Install
```
yarn install
```  
  
###Build (backend bundle)
```
yarn build
```  
  
###Run frontend client
```
yarn start
```  
  
Dev server will be accesible on [http://localhost:8000/](http://localhost:8000/)
